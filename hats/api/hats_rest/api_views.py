from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder

from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href"
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "fabric", "style", "color", "picture_url"]

    def get_extra_data(self, o):
        return {
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number
        }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture_url"
    ]

    def get_extra_data(self, o):
        # return {"location": o.location.closet_name}
        return {
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number
        }

    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        # print("xxxxxxx", hats)
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else: # POST
        content = json.loads(request.body)
        # print("content: ", content)
        try:
            location_href = content["location"]
            # print("location_href: ", location_href)
            location = LocationVO.objects.get(import_href=location_href)
            print("location: ", location)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_show_hat(request,pk):
    try:
        hat = Hat.objects.get(id=pk)
        hat.delete()
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    except Hat.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
