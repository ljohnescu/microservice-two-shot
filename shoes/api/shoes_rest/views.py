from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your encoders here:
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "name", "color", "picture_url"]
    
    
    def get_extra_data(self, o):
        print(o.bin)
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "name", "color", "picture_url"]
    encoders = {"bin": BinVOEncoder() }

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}




# Create your views here:
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        print("THIS IS THE REQUEST", request)
        
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
            print("xxxxxxx", shoes)
        
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            )
    else:
        content=json.loads(request.body)
        print(content)
        try:
            bin_href = content["bin"]
            print(bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
            
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin id"},
                status=400, 
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )



@require_http_methods(["DELETE"])
def api_show_shoes(request, pk):
    try:
        shoes = Shoe.objects.get(id=pk)
        shoes.delete()
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    except Shoe.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})


