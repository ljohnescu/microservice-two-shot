import { Link } from 'react-router-dom';


function MainPage() {
  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Need to keep track of your shoes and hats? We have
            the solution for you!
          </p>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <Link to="shoes">
              <img alt="shoes" width="400px" src="https://images.unsplash.com/photo-1491553895911-0055eca6402d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1480&q=80" />
            </Link>
          </div>
          <div class="col-sm">
            <Link to="hats">
              <img alt="hats" width="400px" src="https://images.unsplash.com/photo-1576529598261-96e376f6aabb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1480&q=80" />
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default MainPage;
