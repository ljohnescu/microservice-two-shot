import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));

async function loadShoes() {
  const response = await fetch("http://localhost:8080/api/shoes");

  if (response.ok) {
    const data = await response.json();
    root.render(<App shoes={data.shoes} />);
  } else {
    console.log("error!!!");
  }
}

loadShoes();


