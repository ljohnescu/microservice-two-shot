import React from "react";
import { Link } from "react-router-dom";

class HatsList extends React.Component {
    constructor() {
        super();
        this.state = { hats: [] };
        this.deleteHat = this.deleteHat.bind(this);
    }

    async componentDidMount() {
        const response = await fetch ('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            this.setState({ hats: data.hats });
        } else {
            console.error(response)
        }
    }

    async deleteHat(event) {
        const url = `http://localhost:8090/api/hats/${event.target.value}`
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            this.setState({hats: this.state.hats.filter(hat => hat.id != event.target.value )});
        } else {
            throw new Error("Error: response not okay")
        }
    }

    render() {
        return (
        <React.Fragment>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link className="btn btn-outline-primary btn-lg px-4 gap-3" to="/hats/new">Add a new hat</Link>
            </div>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Closet</th>
                    <th>Section</th>
                    <th>Shelf</th>
                    <th>Delete?</th>
                </tr>
                </thead>
                <tbody>
                {this.state.hats.map(hat => {
                    return (
                    <tr key={ hat.id }>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style }</td>
                        <td>{ hat.color }</td>
                        <td><img src={hat.picture_url} alt="hat picture" width="100px"/></td>
                        <td>{ hat.closet_name }</td>
                        <td>{ hat.section_number }</td>
                        <td>{ hat.shelf_number }</td>
                        <td>
                            <button className="btn btn-outline-danger" value={hat.id} onClick={this.deleteHat.bind(this)}>Delete</button>
                        </td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </React.Fragment>
        );
    }
}

export default HatsList;