import React from "react";
import { Link } from "react-router-dom";

function ShoeList(props) {
  const deleteShoe = async (id) => {
    const url = `http://localhost:8080/api/shoes/${id}`;
    console.log("hello", url);
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      window.location.reload(false);
    }
  };

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.name}</td>
                <td>{shoe.color}</td>
                <td>
                  <img
                    src={shoe.picture_url}
                    alt=""
                    width="100px"
                    height="100px"
                  />
                </td>
                <td>{shoe.bin}</td>
                <td>
                  <button
                    onClick={() => deleteShoe(shoe.id)}
                    className="btn btn-outline-danger btn-md px-4 gap-3"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-end">
        <Link
          to="/shoes/new"
          className="btn btn-outline-dark btn-md px-4 gap-3"
        >
          Add a shoe
        </Link>
      </div>
    </div>
  );
}

export default ShoeList;
